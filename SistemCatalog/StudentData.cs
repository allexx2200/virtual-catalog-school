﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemCatalog {
    public abstract class Contraint<T> {
        public abstract bool it_works(T value);
    }

    public class InvalidData : Exception{
        public InvalidData(string error_msg) {
            Console.Error.WriteLine(error_msg);
        }

        public InvalidData(string error_msg, string path_to_logfile){
            System.IO.File.WriteAllText(path_to_logfile, error_msg);
        }
    }

    /* Data class defining the fields of the Student Info */
    public class StudentInfo<T> {
        /* current field name */
        protected string field_name;
        protected T data;
        protected List<Contraint<T>> constraint_list;

        /*
         * Basic constructor of the data field
         * @param field_name identificator for the used field
         */
        public StudentInfo(string field_name, T data) {
            this.field_name = field_name;
            this.data = data;
            constraint_list = new List<Contraint<T>>();
        }
        /*
         * Getter function for the field name
         * @return the name of the field
         */
        public string get_name() {
            return field_name;
        }

        /*
         * Setter function for the field name
         * @param field_name the identificator to be set to the current field
         */
        public void set_name(string field_name) {
            this.field_name = field_name;
        }

        /*
         * Getter function for the field data
         * @return the data of the field
         */
        public T get_data() {
            return data;
        }

        /*
         * Setter function for the field data
         * @param field_name the data to be set to the current field
         */
        public void set_data(T data) {
            foreach (Contraint<T> c in constraint_list) {
                if (!c.it_works(data)) {
                    throw new InvalidData("Invalid data has been entered!");
                }
            }
            this.data = data;
        }

        /*
         * Function that adds the constraint to the data
         * @param constraint The constraint class to be added
         */
        public void add_constraint(Contraint<T> constraint) {
            constraint_list.Add(constraint);
        }

        /*
         * Function that removes the constraint to the data
         * @param constraint The constraint class to be removed
         */
        public void remove_constraint(Contraint<T> constraint) {
            constraint_list.Remove(constraint);
        }

        public override string ToString() {
            return field_name + ": " + data.ToString();
        }

    }
    
    public class StudentData {
        
    }
}
