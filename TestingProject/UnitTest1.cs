﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SistemCatalog;
namespace TestingProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void create_catalog() {
            CatalogClient catalog = new CatalogClient();
        }

        [TestMethod]
        public void create_field() {
            StudentInfo<string> info = new StudentInfo<string>("name", "Alex");
            Assert.AreEqual(info.ToString(), "name: Alex");

            StudentInfo<int> info2 = new StudentInfo<int>("age", 21);
            Assert.AreEqual(info2.ToString(), "age: 21");
            Assert.AreEqual(info2.get_data(), 21);
        }


        private class Constraint1 : Contraint<int> {
            public override bool it_works(int value) {
                return value > 1920 && value < 2020;
            }
        }

        [TestMethod]
        public void constraint_field() {
            StudentInfo<int> year = new StudentInfo<int>("Birth year", 1945);
            Constraint1 c = new Constraint1();
            
            year.add_constraint(c);
            Assert.AreEqual(1945, year.get_data());
            Assert.AreEqual("Birth year: 1945", year.ToString());
            year.set_data(1990);
            Assert.AreEqual(1990, year.get_data());
            Assert.AreEqual("Birth year: 1990", year.ToString());
            try {
                year.set_data(1830);
            } catch (Exception e) {}
            Assert.AreEqual(1990, year.get_data());
            Assert.AreEqual("Birth year: 1990", year.ToString());
            year.remove_constraint(c);
            year.set_data(1830);
            Assert.AreEqual(1830, year.get_data());
            Assert.AreEqual("Birth year: 1830", year.ToString());
        }

    }
}
